import './App.css'
import {TituloEncabezado} from './components/Titulo'
import {Buscador} from './components/Buscador'
import {EmojiList} from './components/EmojiList'
import {useState, useEffect} from 'react'


let LIMIT = 28;
let URL = `http://localhost:3001/emojis?_limit=${LIMIT}`;


function App() {
  let [emojis,setEmojis]=useState([])
  let [busqueda,setBusqueda]=useState('')
  let emojiFilter=emojis.filter((emoji)=>{
    if(emoji.title.toLowerCase().includes(busqueda.toLowerCase())){
      return emoji
    }
  })
  useEffect(()=>{
    /*async function obtnerDatos(){
      let respuesta = await fetch(URL)
      let datos = await respuesta.json()
      setEmojis(datos)
    } obtnerDatos()*/
    fetch(URL)
    .then(res=>res.json())
    .then((datos)=>{
      setEmojis(datos)
    })
  },[])

  function actualizarInput(evento){
    let valorInput=evento.target.value
    setBusqueda(valorInput)    
  }
 
  return (
    <div className="container">     
       <TituloEncabezado/>        
       <Buscador valorInput={busqueda} onInputChange={actualizarInput} />       
       <EmojiList datos={busqueda ? emojiFilter : emojis}/>
    </div>
  )
}
export default App
