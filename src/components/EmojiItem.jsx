export function EmojiItem({title,symbol, keywords}){
    return (
        <div className="card">
            <h5 className="card-header text-center"> {title}</h5>
                <div className="card-body text-center">
                    <h2>{symbol}</h2>                    
                    <p className="card-text">{keywords}</p>
                </div>            
        </div>
    );
}