import {EmojiItem} from './EmojiItem'
export function EmojiList({datos}){
    // Hacer optimización 
    let renderEmojis=datos.slice(0,500).map((emoji)=>{
        return (
        <div className='col-3' key={emoji.title}>
            <EmojiItem title={emoji.title}
             symbol={emoji.symbol} 
             keywords={emoji.keywords} />
        </div>)
    })
    return (              
        <div className='row py-4'>            
                {renderEmojis}               
        </div>        
    );
}